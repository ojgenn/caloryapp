angular.module('myApp.services', [])
  .factory("Auth", ["$firebaseAuth",
    function($firebaseAuth) {
      return $firebaseAuth();
    }
  ])
  .service('User', function() {

    return {
      token: '',
      getCaloryLimit(obj){
        let base;
        if(obj.formula == 0) {
          base = 10 * obj.weight + 6.25 * obj.growth - 5 * obj.age;
          if(obj.sex == 0) {
            base -=  161;
          } else {
            base +=  5;
          }
        } else {
          if(obj.sex == 0) {
            base = 66.5 + 13.75 * obj.weight + 5.003 * obj.growth - 6.775 * obj.age;
          } else {
            base = 655.1 + 9.563 * obj.weight + 1.85 * obj.growth - 4.676 * obj.age;
          }
        }
        return Math.round(0.8 * base * obj.exercise);
      },
      setToken(value){
        token = value;
      },
      getToken(){
        if(token.length > 0) return token;
        else return false;
      }
    }
})
  .service('MyFunc', function () {
    return {
      getDate(str) {
        let m = str.getMonth();
        if(m<10) {
          m=0 + '' + (m + 1);
        }
        let day = str.getDate();
        if(day<10) {
          day=0 + '' + day;
        }
        let y = str.getFullYear();
        let value = y + '' + m + '' + day;
        return [y, m, day, value];
      }
    }
});
