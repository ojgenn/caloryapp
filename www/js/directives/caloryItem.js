angular.module('myApp')
  .directive('calory',
    ($ionicModal, $firebaseArray, $ionicPopover) => {
      return {
        restrict: 'E',
        scope: {
        },
        templateUrl: 'templates/calory-item.html',
        link: (scope, el, attrs) => {

          $ionicPopover.fromTemplateUrl('calory-popover.html', {
            scope: scope,
          }).then(function(popover) {
            scope.popover = popover;
          });

          scope.openPopover = function($event) {
            scope.popover.show($event);
          };
          scope.closePopover = function() {
            scope.popover.hide();
          };
          //Cleanup the popover when we're done with it!
          scope.$on('$destroy', function() {
            scope.popover.remove();
          });

          scope.currentItemID = -1;

          $ionicModal.fromTemplateUrl('templates/calory-modal.html', {
            scope: scope,
            animation: 'slide-in-up',
            focusFirstInput: true
          }).then((modal) => {
            scope.modal = modal;
          });

          scope.dailyData = {};
          scope.model = {};
          scope.data = {};
          scope.arr = {};
          scope.sum = 0;

          scope.choseProduct = (item) => {
            scope.model.chosenProductName = item.name;
            scope.model.chosenProductCalory = item.value;
          };

          scope.clearChosenProduct = () => {
            scope.model.chosenProductName = '';
            scope.model.chosenProductCalory = 0;
          };

          scope.calculateCalory = () => {
            scope.model.caloryValue = Math.round(scope.model.chosenProductCalory / 100 * scope.model.productQuantity);
          };

          scope.editItem = (item) => {
            console.log(scope.dailyData)
            scope.currentItemID = Number(item);
            if (item >= 0) {
              scope.model = {
                caloryPrev: Number(scope.arr[item].value),
                caloryValue: Number(scope.arr[item].value),
                item: item,
                name: scope.arr[item].name
              };
            } else if(item == -1){
              scope.model.caloryValue = '';
            } else {
              const recipeRef = db.ref().child("recipeBook/" + scope.dailyData.token).orderByChild('name');
              scope.recipeList = $firebaseArray(recipeRef);
            }
            scope.modal.show();
            scope.popover.hide();
          };


          scope.closeModal = () => {
            scope.modal.hide().then(() => {
              scope.popover.hide();
              scope.currentItemID = -1;
              scope.model = {}
            })
          };

          attrs.$observe('data', (value) => {
            scope.sum = 0;
            scope.dailyData = JSON.parse(value);
            let path = "сaloriesСonsumption/" + scope.dailyData.token + '/' + scope.dailyData.today + '/' + scope.dailyData.foodIntake;
            let ref = db.ref().child(path);
            scope.arr = $firebaseArray(ref);
            scope.arr.$loaded(
              ()=> {
                let sum = 0;
                for(let key in scope.arr){
                  if(typeof (scope.arr[key].value) == "number"){
                    sum += scope.arr[key].value;
                  }
                }
                scope.sum = sum;
              }, (error) => {
                console.error("Error:", error);
              });
            }
          );

          scope.submit = () => {
            if(scope.currentItemID >= 0) {
              scope.arr[scope.currentItemID].name = scope.model.name;
              scope.arr[scope.currentItemID].value = scope.model.caloryValue;
              scope.arr.$save(scope.currentItemID).then(() => {
                scope.sum += scope.model.caloryValue;
                scope.sum -= scope.model.caloryPrev;
                scope.closeModal();
              })
            } else {
              if(!scope.model.chosenProductName){
                scope.model.chosenProductName = 'unnamed';
              }
              scope.arr.$add({name: scope.model.chosenProductName,value: scope.model.caloryValue}).then(() => {
                scope.sum += scope.model.caloryValue;
                scope.modal.hide();
              }).then(() => {
                scope.closeModal();
              });
            }
          };

          scope.deleteItem = (item) => {
            item = Number(item);
            console.log(scope.arr[item].value)
            scope.sum -= scope.arr[item].value;
            scope.arr.$remove(item).then(() => {
            });
          };
        }
      }
    });
