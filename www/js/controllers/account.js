angular.module('myApp.controllers')

  .controller('AccountCtrl', function($scope, $firebaseAuth, $firebaseObject, User, $rootScope, $ionicModal) {
    $scope.userLogged = false;
    $scope.register = 0;
    $scope.model = {};
    $scope.errMsg = '';
    $scope.data = {};

    $scope.authObj = $firebaseAuth();

    let firebaseUser = $scope.authObj.$getAuth();

    if (firebaseUser) {
      $scope.userLogged = true;
      console.log("Signed in as:", firebaseUser.uid);
      //foo.variable = firebaseUser.uid;
      User.setToken(firebaseUser.uid);
    } else {
      $scope.userLogged = false;
      console.log("Signed out");
    }

    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope,
      animation: 'slide-in-up',
      focusFirstInput: true
    }).then((modal) => {
      $scope.modal = modal;
    });

    $scope.closeModal = () => {
      $scope.modal.hide().then(() => {

      })
    };

    $scope.signInRegister = (value) => {
      if(value){
        $scope.register = 1;
      } else {
        $scope.register = 0;
      }

      $scope.modal.show();
    }

    $scope.submit = () => {
      $scope.errMsg = '';
      if($scope.register == 1) {
        $scope.authObj.$createUserWithEmailAndPassword($scope.model.email, $scope.model.password).then(function(firebaseUser) {
          $scope.userLogged = true;
          User.setToken(firebaseUser.uid);
        }).catch(function(error) {
          $scope.userLogged = false;
          $scope.errMsg = 'Ошибка регистрации: ' + error;
        });

      } else {
        $scope.authObj.$signInWithEmailAndPassword($scope.model.email, $scope.model.password).then(function(firebaseUser) {
          console.log("Signed in as:", firebaseUser.uid);
          $scope.userLogged = true;
          User.setToken(firebaseUser.uid);
        }).catch(function(error) {
          $scope.userLogged = false;
          $scope.errMsg = 'Ошибка авторизации: ' + error;
        });
      }

      $scope.closeModal();
    };


    $scope.authObj.$onAuthStateChanged(function(firebaseUser) {
      if (firebaseUser) {
        $scope.userLogged = true;
        console.log("Залогинился как:", firebaseUser.uid);
        let syncObject;
        let ref = db.ref().child("user/" + firebaseUser.uid);
        syncObject = $firebaseObject(ref);
        syncObject.$bindTo($scope,"data").then(function () {
          $rootScope.caloryLimit = User.getCaloryLimit($scope.data);
        })
      } else {
        $scope.userLogged = false;
      }
    });




    $scope.$watchCollection("data", function() {
        $rootScope.caloryLimit = User.getCaloryLimit($scope.data);
      }
    );


    $scope.signOut = () => {
      $scope.authObj.$signOut().then(() => {
        $scope.userLogged = false;
        $scope.errMsg = 'Войдите или зарегистрируйтесь';
      });
    };

  });
