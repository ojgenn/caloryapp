angular.module('myApp.controllers')

  .controller('CaloryCtrl', function($scope, $firebaseArray, MyFunc, $state, User) {

    let searchArr = ['breakfast', 'lunch', 'dinner', 'exercises'];
    $scope.token = User.getToken();

    if (!$scope.token) {
      $state.go('tab.account');
    }

    $scope.$on('$ionicView.enter', () => {

      $scope.date = new Date();
      $scope.today = MyFunc.getDate($scope.date);

      $scope.data = {};
      $scope.values = {breakfast: 0, lunch: 0, dinner: 0, exercises: 0};

      $scope.$watch('today', () => {
        const ref = db.ref().child("сaloriesСonsumption/" + $scope.token + '/' + $scope.today[3]);
        let syncObject = $firebaseArray(ref);
        syncObject.$watch(function(event) {
          $scope.values = {breakfast: 0, lunch: 0, dinner: 0, exercises: 0};
          for (let key in syncObject){
            if (searchArr.indexOf(syncObject[key].$id) >= 0) {
              let energyFlow = syncObject[key].$id;
              $scope.values[energyFlow] = 0;
              let energyItems = syncObject[key];
              for (let item in energyItems) {
                if (energyItems[item] != null && typeof(energyItems[item]) == 'object') {
                  $scope.values[energyFlow] += energyItems[item].value;
                }
              }
            }
          }
        });
      });

      $scope.setDate = (str) => {
        let a = $scope.date.getTime();
        let b;
        if (str == 'yesterday') {
          b = new Date(a - 1000 * 60 * 60 * 24);
        } else {
          b = new Date(a + 1000 * 60 * 60 * 24);
        }
        $scope.date = b;
        $scope.today = MyFunc.getDate($scope.date);
        $scope.values = {breakfast: 0, lunch: 0, dinner: 0, exercises: 0};
      };


    })
  });
