angular.module('myApp.controllers')

  .controller('RecipesCtrl', function($scope, $ionicModal, $firebaseArray, MyFunc, $state, User) {
    let token = User.getToken();
    console.log(token);
    $scope.$on('$ionicView.enter', function(){
      if (!token) {
        $state.go('tab.account');
      }

      $scope.currentItem = -1;
      $scope.model = {};

      const ref = db.ref().child("recipeBook/" + token).orderByChild('name');
      $scope.list = $firebaseArray(ref);

      $ionicModal.fromTemplateUrl('templates/recipe-modal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
      }).then((modal) => {
        $scope.modal = modal;
      });

      $scope.closeModal = () => {
        $scope.modal.hide().then(() => {
          $scope.currentItem = -1;
          $scope.model = {};
        })
      };

      $scope.editProduct = (value) => {
        value = Number(value);
        if(value >= 0){
          $scope.currentItem = value;
          $scope.model.name = $scope.list[value].name;
          $scope.model.value = $scope.list[value].value;
          $scope.model.id = $scope.list[value].id;

        }
        $scope.modal.show();
      };

      $scope.submit = () => {
        if($scope.currentItem >= 0) {
          $scope.list[$scope.currentItem].name = $scope.model.name;
          $scope.list[$scope.currentItem].value = $scope.model.value;
          $scope.list.$save($scope.currentItem)
            .then(function(ref) {
              //ref.key === list[2].$id;
          });

        } else {
          let obj = {name: $scope.model.name,value:$scope.model.value}
          $scope.list.$add(obj).then(function (ref) {
            obj.id = ref.key;
            }
          );
        }
        $scope.closeModal();
      }

      $scope.deleteProduct = (value) => {
        value = Number(value);
        $scope.list.$remove(value).then(function(ref) {
        });
      }

    });
  });

