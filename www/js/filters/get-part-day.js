angular.module('myApp')
  .filter('getPartName', function () {
    return (str) => {
      let obj = {breakfast: "Завтрак", lunch: "Обед", dinner: "Ужин"};
      for(let key in obj) {
        if(key == str) {
          return obj[key];
        }
      }
      return "Упражнения";
    }
  })
