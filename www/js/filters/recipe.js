angular.module('myApp')
  .filter('search', function() {
    return function(input, searchText) {
      if(searchText) {
        let obj = {};
        for(let key in input){
          if(typeof (input[key].name) == 'string'){
            if(input[key].name.indexOf(searchText) >= 0) {
              obj[key] =  input[key];
            }
          }
        }
        input = obj;
      }
      return input;
    }
  })
