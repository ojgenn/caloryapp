angular.module('myApp')
  .filter('mydate', function () {
    return function (input) {
      let date = new Date();
      let today = date.getFullYear() + '.' + (date.getMonth() + 1) + '.' + date.getDate();
      today = new Date(today).getTime();
      let inputDay = input.split('.');
      inputDay =  new Date(inputDay[2] + '.' + inputDay[1] + '.' + inputDay[0]).getTime();
      let diff = today - inputDay;
      switch (diff) {
        case 0:
          input = 'Сегодня';
          break;
        case 864e5:
          input = 'Вчера';
          break;
        case 864e5*2:
          input = 'Позавчера';
          break;
        case -864e5:
          input = 'Завтра';
          break;
        case -864e5*2:
          input = 'Послезавтра';
          break;
      }
      return input;
    }
  })
